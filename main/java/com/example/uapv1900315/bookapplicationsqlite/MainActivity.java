package com.example.uapv1900315.bookapplicationsqlite;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;


 import android.content.DialogInterface;

 import android.support.v7.app.AlertDialog;

 import android.widget.AdapterView;
 import android.widget.Toast;
 public class MainActivity extends AppCompatActivity {
     ListView lv;
     BookDbHelper db;
     SimpleCursorAdapter adapter;




     @Override protected void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_main);
         Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
         setSupportActionBar(toolbar);

         db = new BookDbHelper(getApplicationContext());

        //db.populate();

       /*  Cursor result=db.getReadableDatabase().query(BookDbHelper.TABLE_NAME,
                 new String[] {"_id AS _id", BookDbHelper.COLUMN_BOOK_TITLE, BookDbHelper.COLUMN_AUTHORS,BookDbHelper.COLUMN_YEAR,BookDbHelper.COLUMN_GENRES,BookDbHelper.COLUMN_PUBLISHER},
                 null, null, null, null, BookDbHelper.COLUMN_BOOK_TITLE);*/
         Cursor result =db.fetchAllBooks();
         // Declaration of an adapter with items with 2 elements
         adapter = new SimpleCursorAdapter(MainActivity.this, android.R.layout.simple_list_item_2,
                 result, new String[] { BookDbHelper.COLUMN_BOOK_TITLE, BookDbHelper.COLUMN_AUTHORS },
                 new int[] { android.R.id.text1, android.R.id.text2 }, 0);
         // Associate the adapter with the list view
         lv = (ListView) findViewById(R.id.listview);


         registerForContextMenu(lv);
         adapter.changeCursor(db.fetchAllBooks());
         adapter.notifyDataSetChanged();
         lv.setAdapter(adapter);
         lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

             public void onItemClick(AdapterView<?> parent, View view,
                                     int position, long id) {

                 Intent myIntent = new Intent(view.getContext(), BookActivity.class);
                 //String countryname =(String) (lv.getItemAtPosition(position));
                 Cursor item = (Cursor)lv.getItemAtPosition(position);
                 Book book=db.cursorToBook(item);
                 Long ide=book.getId();

                 myIntent.putExtra("book", book);
                 startActivityForResult(myIntent, 0);

             }
         });





      /*   lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
             @Override public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int position, long l)
             {
                 final CharSequence[] items = {"Delete"}; AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
             dialog.setItems(items, new DialogInterface.OnClickListener() {
                 @Override public void onClick(DialogInterface dialogInterface, int i) {
                     if (i == 0){

                     }
            }
             });
             dialog.show(); return true; } });
      */


           FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
           fab.setOnClickListener(new View.OnClickListener() {
             @Override public void onClick(View view) {

                 Intent myIntent = new Intent(view.getContext(), BookActivity.class);
                 startActivityForResult(myIntent, 0);
                                       Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                                               .setAction("Action", null).show(); }
           }); }
             @Override public boolean onCreateOptionsMenu(Menu menu) {
         // Inflate the menu; this adds items to the action bar if it is present. getMenuInflater().inflate(R.menu.menu_main, menu);

                 return true; }
     @Override
     public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
     {
         // TODO: Implement this method
         super.onCreateContextMenu(menu, v, menuInfo);

         MenuInflater inflater = getMenuInflater();
         inflater.inflate(R.menu.context_menu, menu);


     }
           @Override public boolean onOptionsItemSelected(MenuItem item) {
         // Handle action bar item clicks here. The action bar will
               // automatically handle clicks on the Home/Up button, so long
               // as you specify a parent activity in AndroidManifest.xml.
               int id = item.getItemId();
               //noinspection SimplifiableIfStatement
               if (id == R.id.action_settings) { return true; }
               return super.onOptionsItemSelected(item);
             }
             @Override

     public boolean   onContextItemSelected(MenuItem item) {
         AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

         switch(item.getItemId()) {

         case R.id.supprimer: Cursor iteme = (Cursor)lv.getItemAtPosition(info.position);

         Book book=db.cursorToBook(iteme); db.deleteBook(iteme); adapter.changeCursor(db.fetchAllBooks());

             adapter.notifyDataSetChanged();; return true;
         default:return super.onOptionsItemSelected(item);
     }
     }
     @Override
     protected void onResume() {

         super.onResume();

         //adapter=db.fetchAllBooks();
         adapter.changeCursor(db.fetchAllBooks());
         adapter.notifyDataSetChanged();

     }

     }





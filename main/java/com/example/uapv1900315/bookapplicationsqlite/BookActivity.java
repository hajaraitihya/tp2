package com.example.uapv1900315.bookapplicationsqlite;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class BookActivity extends AppCompatActivity {

    EditText namebook;
    EditText authors;
    EditText year;
    EditText genre;
    EditText publisher;
    Button sauvgarde;
    BookDbHelper db;
    Book book;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db= new BookDbHelper(getApplicationContext());
        setContentView(R.layout.activity_book);
        namebook=(EditText)findViewById(R.id.nameBook);
        authors=(EditText)findViewById(R.id.editAuthors);
        year=(EditText)findViewById(R.id.editYear);
        genre=(EditText)findViewById(R.id.editGenres);
        publisher=(EditText)findViewById(R.id.editPublisher);
        sauvgarde=(Button)findViewById(R.id.button) ;


// >>jfhfh


        Bundle extras = getIntent().getExtras();















        if(extras!=null){{
            if(extras.containsKey("book"))


            book =(Book) extras.get("book");

        if(book!=null){
           namebook.setText(book.getTitle());
           authors.setText(book.getAuthors());
           year.setText(book.getYear());
           genre.setText(book.getGenres());
            publisher.setText(book.getPublisher());
            sauvgarde.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    book.setTitle(namebook.getText().toString());
                    book.setAuthors(authors.getText().toString());
                    book.setYear(year.getText().toString());
                    book.setGenres(genre.getText().toString());
                    book.setPublisher(publisher.getText().toString());
                    db.updateBook(book);
                    Intent myIntent = new Intent(BookActivity.this, MainActivity.class);
                    startActivityForResult(myIntent, 0);
                    Toast.makeText(v.getContext(),"suavgarded ",Toast.LENGTH_LONG).show();
                }
            });} }
        }else {
            sauvgarde.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    // TODO Auto-generated method stub
                    book= new Book();
                    Cursor c=db.fetchAllBooks();
                    List<Book> listbook=new  ArrayList<Book>();
                    if (c.moveToFirst()) {
                        do {
                            Book booke = new Book();
                            booke.setId(c.getInt((0)));
                            booke.setTitle((c.getString(1)));
                            booke.setAuthors((c.getString(2)));
                            booke.setYear(c.getString(3));
                            booke.setGenres(c.getString(4));
                            booke.setPublisher(c.getString(5));
                            // adding to todo list
                            listbook.add(booke);
                        } while (c.moveToNext());
                    }
                    boolean isexist=false;
                    for(int i=0;i<listbook.size();i++){
                        if(namebook.getText().toString().equals(listbook.get(i).getTitle())){
                            isexist=true;
                        }
                    }
                    book.setTitle(namebook.getText().toString());
                    book.setAuthors(authors.getText().toString());
                    book.setYear(year.getText().toString());
                    book.setGenres(genre.getText().toString());
                    book.setPublisher(publisher.getText().toString());
                    if(namebook.getText().toString().equals("Name")||namebook.getText().toString().equals("")){
                        AlertDialog alertDialog = new AlertDialog.Builder(BookActivity.this).create();
                        alertDialog.setTitle("attention");
                        alertDialog.setMessage("the name can not be empty");
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
                    }else if(isexist==true){
                        AlertDialog alertDialog = new AlertDialog.Builder(BookActivity.this).create();
                        alertDialog.setTitle("attention");
                        alertDialog.setMessage("Déja exist ce nom");
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();

                    }
                    else{
                    db.addBook(book);
                    Intent myIntent = new Intent(BookActivity.this, MainActivity.class);
                    startActivityForResult(myIntent, 0);
                    Toast.makeText(v.getContext(),"suavgarded ",Toast.LENGTH_LONG).show();}
                }
            });

        }


    }
}
